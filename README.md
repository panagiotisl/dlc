# About #

Dispersion-aware Link Communities (DLC) algorithm for finding overlapping communities in complex networks.
The code uses the implementation of [1] found in [2] to perform a comparison between DLC and Link Communities (LC) algorithn.


# Execution #

Requires [LRU Dict](https://github.com/amitdev/lru-dict)

    pip install lru-dict

or

    easy_install lru_dict

Execute an example with the following:

    python lesmiserables.py LesMiserablesTestCase



### References ###

1. Yong-Yeol Ahn, James P. Bagrow, and Sune Lehmann, Link communities reveal multiscale complexity in networks, Nature 466, 761 (2010).

2. https://github.com/bagrow/linkcomm

### Who do I talk to? ###

Panagiotis Liakos: [Personal Website](http://www.di.uoa.gr/~grad0990).
