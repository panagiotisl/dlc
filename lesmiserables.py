"""A test suite for the network of Les Miserables"""


import unittest
import link_clustering
import dispersion
import measures
import os


class LesMiserablesTestCase(unittest.TestCase):
    def test_with_dispersion(self):
        delimiter = '\t'
        threshold = None
        is_weighted = False
        dendro_flag = False

        edgelist_filename = 'lesmiserables/lesmiserables.txt'

        print "# measuring using dispersion"
        print "# loading network from edgelist..."
        basename = os.path.splitext(edgelist_filename)[0]
        if is_weighted:
            adj, edges, ij2wij = link_clustering.read_edgelist_weighted(edgelist_filename, delimiter=delimiter)
        else:
            adj, edges = link_clustering.read_edgelist_unweighted(edgelist_filename, delimiter=delimiter)

        Disp, max_disp, min_disp = dispersion.get_recursive_dispersion(adj)

        disp_scaled = link_clustering.get_scaled_dispersion(Disp, max_disp, min_disp)
        # run the method:
        if threshold is not None:
            if is_weighted:
                edge2cid, D_thr = link_clustering.HLC(adj, edges).single_linkage(threshold, w=ij2wij)
            else:
                edge2cid, D_thr = link_clustering.HLC(adj, edges).single_linkage(threshold, disp_scaled=disp_scaled)
            print "# D_thr = %f" % D_thr
            clusters = link_clustering.write_edge2cid(edge2cid, "%s_thrS%f_thrD%f" % (basename, threshold, D_thr), delimiter=delimiter)
            clusters = measures.get_non_trivial_clusters(clusters)
        else:
            if is_weighted:
                edge2cid, S_max, D_max, list_D = link_clustering.HLC(adj, edges).single_linkage(w=ij2wij)
            else:
                if dendro_flag:
                    edge2cid, S_max, D_max, list_D, orig_cid2edge, linkage = link_clustering.HLC(adj, edges).single_linkage(
                        dendro_flag=dendro_flag)
                    link_clustering.write_dendro("%s_dendro" % basename, orig_cid2edge, linkage)
                else:
                    edge2cid, S_max, D_max, list_D = link_clustering.HLC(adj, edges).single_linkage(disp_scaled=disp_scaled)

            f = open("%s_thr_D.txt" % basename, 'w')
            for s, D in list_D:
                print >> f, s, D
            f.close()
            print "# D_max = %f\n# S_max = %f" % (D_max, S_max)
            clusters = link_clustering.write_edge2cid(edge2cid, "%s_maxS%f_maxD%f" % (basename, S_max, D_max), delimiter=delimiter)
            clusters = measures.get_non_trivial_clusters(clusters)

        self.assertEqual(22, len(clusters))
        cp = measures.get_community_coverage(adj, clusters)
        print "coverage percentage: ", cp

        mo = measures.get_overlap_coverage(adj, clusters)
        print "mean overlap: ", mo

        for cluster in clusters:
            print cluster

    def test_without_dispersion(self):
        delimiter = '\t'
        threshold = None
        is_weighted = False
        dendro_flag = False
        edgelist_filename = 'lesmiserables/lesmiserables.txt'

        print "# measuring without dispersion"
        print "# loading network from edgelist..."
        basename = os.path.splitext(edgelist_filename)[0]
        if is_weighted:
            adj, edges, ij2wij = link_clustering.read_edgelist_weighted(edgelist_filename, delimiter=delimiter)
        else:
            adj, edges = link_clustering.read_edgelist_unweighted(edgelist_filename, delimiter=delimiter)
        # run the method:
        if threshold is not None:
            if is_weighted:
                edge2cid, D_thr = link_clustering.HLC(adj, edges).single_linkage(threshold, w=ij2wij)
            else:
                edge2cid, D_thr = link_clustering.HLC(adj, edges).single_linkage(threshold, with_dispersion=False)
            print "# D_thr = %f" % D_thr
            clusters = link_clustering.write_edge2cid(edge2cid, "%s_thrS%f_thrD%f" % (basename, threshold, D_thr), delimiter=delimiter)
            clusters = measures.get_non_trivial_clusters(clusters)
        else:
            if is_weighted:
                edge2cid, S_max, D_max, list_D = link_clustering.HLC(adj, edges).single_linkage(w=ij2wij)
            else:
                if dendro_flag:
                    edge2cid, S_max, D_max, list_D, orig_cid2edge, linkage = link_clustering.HLC(adj, edges).single_linkage(
                        dendro_flag=dendro_flag, with_dispersion=False)
                    link_clustering.write_dendro("%s_dendro" % basename, orig_cid2edge, linkage)
                else:
                    edge2cid, S_max, D_max, list_D = link_clustering.HLC(adj, edges).single_linkage(with_dispersion=False)

            f = open("%s_thr_D.txt" % basename, 'w')
            for s, D in list_D:
                print >> f, s, D
            f.close()
            print "# D_max = %f\n# S_max = %f" % (D_max, S_max)
            clusters = link_clustering.write_edge2cid(edge2cid, "%s_maxS%f_maxD%f" % (basename, S_max, D_max), delimiter=delimiter)
            clusters = measures.get_non_trivial_clusters(clusters)

        self.assertEqual(19, len(clusters))
        cp = measures.get_community_coverage(adj, clusters)
        print "coverage percentage: ", cp

        mo = measures.get_overlap_coverage(adj, clusters)
        print "mean overlap: ", mo

        for cluster in clusters:
            print cluster


if __name__ == '__main__':
    unittest.main()
